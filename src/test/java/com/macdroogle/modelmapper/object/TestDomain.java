package com.macdroogle.modelmapper.object;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class TestDomain
{
    private String name;
    private String address;
    private Date date;
    private boolean status;

    public void setAddress(String address, String addressTwo)
    {
        this.address = address + "," + addressTwo;
    }
}
