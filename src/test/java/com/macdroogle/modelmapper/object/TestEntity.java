package com.macdroogle.modelmapper.object;


import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TestEntity
{
    private String name;
    private String address;
    private Date date;
    private boolean status;
}
