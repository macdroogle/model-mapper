package com.macdroogle.modelmapper;

import com.macdroogle.modelmapper.object.TestDomain;
import com.macdroogle.modelmapper.object.TestEntity;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class ModelMapperTest
{

    private ModelMapper modelMapper = new ModelMapper();

    @Test
    public void testModelMapperMapsObjectWithSameType() throws InstantiationException, IllegalAccessException
    {
        String mappedString = modelMapper.mapObjectToType("Hello", String.class);
        Assert.assertNotNull(mappedString);
    }

    @Test
    public void testModelMapperMapsCustomObject() throws InstantiationException, IllegalAccessException
    {
        TestDomain testDomain = TestDomain.builder().address("myAddress").date(new Date(1234)).name("hello").status(true).build();
        TestEntity mappedEntity = modelMapper.mapObjectToType(testDomain, TestEntity.class);
        Assert.assertNotNull(mappedEntity);
        Assert.assertEquals(testDomain.getAddress(), mappedEntity.getAddress());
        Assert.assertEquals(testDomain.getDate(), mappedEntity.getDate());
        Assert.assertEquals(testDomain.isStatus(), mappedEntity.isStatus());
        Assert.assertEquals(testDomain.getName(), mappedEntity.getName());
    }

}
