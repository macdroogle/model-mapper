package com.macdroogle.modelmapper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;


/**
 * Very simple ModelMapper which will map a mappableObject to a new instance of an object with a
 * passed in type
 */
public class ModelMapper
{

    /**
     * Cache newInstance
     */
    private Object newInstance;

    /**
     * Constructor for instantiating a new ModelMapper Instance
     */
    public ModelMapper()
    {
        // For instantiating a modelMapper instance
    }


    /**
     * Map a mappableObject to a new instance of an object of type {@link Class}
     *
     * @param mappableObject The mappable object
     * @param returnType     The object with which to return a new instance
     * @param <T>            Templated type
     * @return A new instance of an object based on the passed in return type
     * @throws IllegalAccessException On failure to create new instance
     * @throws InstantiationException On failure to create new instance
     */
    public <T> T mapObjectToType(Object mappableObject, Class<T> returnType) throws IllegalAccessException, InstantiationException
    {
        synchronized(this)
        {
            Class classOfMappableObject = mappableObject.getClass();

            if(classOfMappableObject == returnType || returnType.isAssignableFrom(classOfMappableObject))
            {
                return (T) mappableObject;
            }

            Method[] mappableObjectMethods = mappableObject.getClass().getMethods();
            Method[] returnTypeMethods = returnType.getMethods();

            if(newInstance == null)
            {
                newInstance = returnType.newInstance();
            }

            Arrays.stream(mappableObjectMethods).filter(this::isAMethodReturningAnObject)
                .forEach(getterMethod ->
                    Arrays.stream(returnTypeMethods)
                        .filter(method ->
                            isAMethodWithAMatchingNameAndParameters(returnType, getterMethod, method)
                        )
                        .forEach(setterMethod ->
                            executeMethodAgainstInstanceOfReturnableObject(mappableObject, getterMethod, setterMethod, newInstance)
                        )
                );
        }
        return (T) newInstance;
    }

    private boolean isAMethodReturningAnObject(Method method)
    {
        return method.getName().toLowerCase().startsWith("get") || method.getName().toLowerCase().startsWith("is");
    }

    private Object executeMethodAgainstInstanceOfReturnableObject(Object mappableObject, Method getterMethod, Method setterMethod, Object returnableObject)
    {
        try
        {
            setterMethod.invoke(returnableObject, getterMethod.invoke(mappableObject));
        }
        catch(InvocationTargetException | IllegalAccessException ignored)
        {
            // ignored
        }
        return returnableObject;
    }

    private <T> boolean isAMethodWithAMatchingNameAndParameters(Class<T> returnType, Method getterMethodFromMappableObject, Method methodFromObjectToInvoke)
    {
        try
        {
            return (methodFromObjectToInvoke.getName().toLowerCase().startsWith("set"))
                && (getCleanedMethodName(methodFromObjectToInvoke.getName()).equals(getCleanedMethodName(getterMethodFromMappableObject.getName())))
                && Arrays.equals(returnType.getMethod(getterMethodFromMappableObject.getName()).getParameterTypes(), getterMethodFromMappableObject.getParameterTypes());
        }
        catch(NoSuchMethodException e)
        {
            // ignored
        }
        return false;
    }

    private String getCleanedMethodName(String methodName)
    {
        return (methodName.startsWith("get") || methodName.startsWith("set") || methodName.startsWith("is")) ? methodName.replaceFirst("get", "").replaceFirst("set", "").replaceFirst("is", "") : methodName;
    }
}
